package com.ikingssgc.mdc.gui;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ikingssgc.mdc.ModMDC;
import com.ikingssgc.mdc.debug.Logger;
import com.ikingssgc.mdc.proxy.CommonProxyBase;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.settings.GameSettings;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Emir
 */
public class GuiOptionsProxy extends GuiOptions {

    public GuiOptionsProxy(GuiScreen par1GuiScreen, GameSettings par2GameSettings) {
        super(par1GuiScreen, par2GameSettings);
    }

    @Override
    public void initGui() {
        super.initGui();
        /*Iterator it = this.buttonList.iterator();
         while (it.hasNext()) {
         GuiButton but = (GuiButton) it.next();
         if (but.id == 8675309) {
         it.remove();
         }
         }*/
        this.buttonList.add(new GuiButton(201, this.width / 2 + 2, this.height / 6 + 72 - 24 - 6, 150, 20, "Mod Division Core Settings..."));
    }

    @Override
    protected void actionPerformed(GuiButton button) {
        if (button.id == 201) {
            mc.displayGuiScreen(new GuiConfig(this, getJsonObject(), new GuiConfig.SaveEvent() {
                @Override
                public void onJsonSave(JsonObject json) {
                    setJsonObject(json);
                    ModMDC.instance.reloadConfig();
                }
            }));
            return;
        }
        super.actionPerformed(button);
    }

    private void setJsonObject(JsonObject json) {
        try {
            FileUtils.write(CommonProxyBase.jsonFile, new GsonBuilder().setPrettyPrinting().create().toJson(json));
        } catch (IOException ex) {
            Logger.error("cannot write json config file: " + ex);
        }
    }

    private JsonObject getJsonObject() {
        if (!CommonProxyBase.jsonFile.exists()) {
            return initJsonObject();
        }
        try {
            JsonObject ret = new JsonParser().parse(new FileReader(CommonProxyBase.jsonFile)).getAsJsonObject();
            return ret;
        } catch (IOException ex) {
            Logger.error("cannot read json config file: " + ex);
        }
        return initJsonObject();
    }

    private JsonObject initJsonObject() {
        JsonObject ret = new JsonObject();
        ModMDC.proxy.initConfig(ret);
        return ret;
    }
}

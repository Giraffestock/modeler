package com.ikingssgc.mdc.gui;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
//import com.ikingssgc.client.gui.GuiSlotConfig;
import com.ikingssgc.mdc.config.mdcCoreStatics;
import java.io.File;
import java.util.Map;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.opengl.GL11;

//TODO: add support for JsonObjects
public class GuiConfig extends GuiScreen {

    private final GuiScreen parent;
    private final JsonObject settings;
    private final GuiConfig.SaveEvent saveEvent;
    private GuiSlotConfig guiSlotConfig;
    private GuiButtonToggle guiBoolean;
    private GuiDecimalTextField guiNumber;
    private GuiTextField guiString;
    private GuiButton guiJsonValueSave, guiSave;
    private GuiButton guiJsonValueCancel, guiCancel;

    public GuiConfig(GuiScreen parent) {
        JsonObject test = new JsonObject();
        test.addProperty("string", "lalalalalaa");
        test.addProperty("number", 4935345);
        test.addProperty("boolean", true);
        test.addProperty("bool2", false);
        test.addProperty("string2", "lalalalalaa");
        test.addProperty("numbe2r", 4935345);
        test.addProperty("boole22an", true);
        test.addProperty("bool222", false);
        test.addProperty("strin2g", "lalalalalaa");
        test.addProperty("n1umber", 4935345);
        test.addProperty("bo1olean", true);
        test.addProperty("boo1l2", false);
        test.addProperty("stri1ng", "lalalalalaa");
        test.addProperty("num2ber", 4935345);
        test.addProperty("boo3lean", true);
        test.addProperty("bool22", false);
        test.addProperty("stri3ng", "lalalalalaa");
        test.addProperty("numb32er", 4935345);
        test.addProperty("bool54ean", true);
        test.addProperty("boo345l2", false);
        this.settings = test;
        this.saveEvent = null;
        this.parent = parent;
    }

    public GuiConfig(GuiScreen parent, JsonObject settings, GuiConfig.SaveEvent saveEvent) {
        this.settings = settings;
        this.saveEvent = saveEvent;
        this.parent = parent;
    }

    @Override
    public void initGui() {
        int i = 0;
        guiSlotConfig = new GuiSlotConfig(this, mc, settings, width, height);
        for (Map.Entry<String, JsonElement> entry : settings.entrySet()) {
            if (entry.getValue().isJsonPrimitive()) {
                JsonPrimitive prim = entry.getValue().getAsJsonPrimitive();
                if (prim.isBoolean()) {
                    this.buttonList.add(new GuiButtonToggle(prim.getAsBoolean(), i, 20, 20 + (i * 24), entry.getKey().toUpperCase()));
                }
                if (prim.isNumber()) {
                    prim.getAsNumber();
                }
            }
            i++;
        }
        this.buttonList.add(guiSave = new GuiButton(50, (width / 2) + 20, (height - 30), 150, 20, "Save"));
        this.buttonList.add(guiCancel = new GuiButton(51, (width / 2) - 20 - 150, (height - 30), 150, 20, "Cancel"));
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        if (guiSlotConfig.selected == -1) {
            guiSlotConfig.drawScreen(par1, par2, par3);
        }
        if (guiSlotConfig.selected != -1) {
            int w = 130, h = 40;
            drawbox((width / 2) - w, (height / 2) - h, w * 2, h * 2);

            String name = guiSlotConfig.jsonIntegerMap.get(guiSlotConfig.selected);
            JsonPrimitive primitive = guiSlotConfig.jsonMap.get(name);

            drawCenteredString(mc.fontRenderer, "EDITING: " + name.replaceAll("_", " "), (width / 2), (height / 2) - 35, 0xffffffff);

            if (primitive.isBoolean()) {
                if (guiBoolean != null) {
                    guiBoolean.drawButton(mc, par1, par2);
                }
            }
            if (primitive.isNumber()) {
                if (guiNumber != null) {
                    guiNumber.drawTextBox();
                }
            }
            if (primitive.isString()) {
                if (guiString != null) {
                    guiString.drawTextBox();
                }
            }

            guiJsonValueSave.drawButton(mc, par1, par2);
            guiJsonValueCancel.drawButton(mc, par1, par2);
        }

        guiSave.enabled = (guiSlotConfig.selected == -1);
        guiCancel.enabled = (guiSlotConfig.selected == -1);

        if (guiSlotConfig.selected == -1) {
            drawCenteredString(fontRendererObj, "Mod Division Core settings", (width / 2), 12, 0xFFFFFFFF);

            guiSave.drawButton(mc, par1, par2);
            guiCancel.drawButton(mc, par1, par2);
        }
    }

    protected void initInput(int index) {
        String name = guiSlotConfig.jsonIntegerMap.get(index);
        JsonPrimitive primitive = guiSlotConfig.jsonMap.get(name);

        guiJsonValueSave = new GuiButton(20, (width / 2) + 5, (height / 2) + 15, 100, 15, "Save");
        guiJsonValueCancel = new GuiButton(20, (width / 2) - 100 - 5, (height / 2) + 15, 100, 15, "Cancel");

        buttonList.add(guiJsonValueCancel);
        buttonList.add(guiJsonValueSave);

        if (primitive.isBoolean()) {
            guiBoolean = new GuiButtonToggle(primitive.getAsBoolean(), 20, (width / 2) - 100, (height / 2) - 15, name.replaceAll("_", " "));
        }
        if (primitive.isNumber()) {
            guiNumber = new GuiDecimalTextField(mc.fontRenderer, (width / 2) - 100, (height / 2) - 15, 200, 20);
            guiNumber.setText(primitive.getAsNumber() + "");
            guiNumber.setFocused(true);
        }
        if (primitive.isString()) {
            guiString = new GuiTextField(mc.fontRenderer, (width / 2) - 100, (height / 2) - 15, 200, 20);
            guiString.setText(primitive.getAsString() + "");
            guiString.setFocused(true);
        }
    }

    @Override
    protected void actionPerformed(GuiButton gui) {
        this.buttonList.clear();

        this.buttonList.add(guiSave = new GuiButton(50, (width / 2) + 20, (height - 30), 150, 20, "Save"));
        this.buttonList.add(guiCancel = new GuiButton(51, (width / 2) - 20 - 150, (height - 30), 150, 20, "Cancel"));

        if (gui == guiJsonValueSave) {
            String name = guiSlotConfig.jsonIntegerMap.get(guiSlotConfig.selected);
            if (guiBoolean != null) {
                guiSlotConfig.jsonMap.put(name, new JsonPrimitive(guiBoolean.getValue()));
            }
            if (guiNumber != null) {
                guiSlotConfig.jsonMap.put(name, new JsonPrimitive(guiNumber.getValue()));
            }
            if (guiString != null) {
                guiSlotConfig.jsonMap.put(name, new JsonPrimitive(guiString.getText()));
            }
        }

        if (gui.id == 50) {
            if (this.saveEvent != null) {
                JsonObject newobj = new JsonObject();
                for(Map.Entry<String, JsonPrimitive> entry : guiSlotConfig.jsonMap.entrySet()){
                    newobj.add(entry.getKey(), entry.getValue());
                }
                this.saveEvent.onJsonSave(newobj);
            }
            mc.displayGuiScreen(parent);
        }
        if (gui.id == 51) {
            mc.displayGuiScreen(parent);
        }

        guiBoolean = null;
        guiNumber = null;
        guiString = null;

        guiSlotConfig.selected = -1;
    }

    private void drawbox(int x, int y, int width, int height) {
        Tessellator var18 = Tessellator.instance;
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        var18.startDrawingQuads();
        var18.setColorOpaque_I(8421504);
        var18.addVertexWithUV((double) x, (double) (y + height + 2), 0.0D, 0.0D, 1.0D);
        var18.addVertexWithUV((double) x + width, (double) (y + height + 2), 0.0D, 1.0D, 1.0D);
        var18.addVertexWithUV((double) x + width, (double) (y - 2), 0.0D, 1.0D, 0.0D);
        var18.addVertexWithUV((double) x, (double) (y - 2), 0.0D, 0.0D, 0.0D);
        var18.setColorOpaque_I(0);
        var18.addVertexWithUV((double) (x + 1), (double) (y + height + 1), 0.0D, 0.0D, 1.0D);
        var18.addVertexWithUV((double) x + (width - 1), (double) (y + height + 1), 0.0D, 1.0D, 1.0D);
        var18.addVertexWithUV((double) x + (width - 1), (double) (y - 1), 0.0D, 1.0D, 0.0D);
        var18.addVertexWithUV((double) (x + 1), (double) (y - 1), 0.0D, 0.0D, 0.0D);
        var18.draw();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    @Override
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        if (guiBoolean != null) {
            if (par3 == 0) {
                if (guiBoolean.mousePressed(this.mc, par1, par2)) {
                    guiBoolean.func_146113_a(this.mc.getSoundHandler());
                }
            }
        }
        if (guiNumber != null) {
            guiNumber.mouseClicked(par1, par2, par3);
        }
        if (guiString != null) {
            guiString.mouseClicked(par1, par2, par3);
        }
    }

    @Override
    public void updateScreen() {
        if (guiNumber != null) {
            guiNumber.updateCursorCounter();
        }
        if (guiString != null) {
            guiString.updateCursorCounter();
        }
    }

    @Override
    protected void keyTyped(char par1, int par2) {
        super.keyTyped(par1, par2);
        if (guiNumber != null) {
            guiNumber.textboxKeyTyped(par1, par2);
        }
        if (guiString != null) {
            guiString.textboxKeyTyped(par1, par2);
        }
    }

    public static interface SaveEvent {

        public void onJsonSave(JsonObject json);
    }
}

package com.ikingssgc.mdc.gui.elements;

import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import com.ikingssgc.mdc.utils.RenderUtils;
import net.minecraft.client.Minecraft;


public class GallifreyanSymbols {
	/*
	public static void renderCircle1(TextureManager renderEngine, int x, int y){
    	GL11.glBindTexture(GL11.GL_TEXTURE_2D, renderEngine.getTexture("/guiTardisFlight.png"));
        GL11.glPushMatrix();

        GL11.glTranslatef(x - 60, y - 70, 0);
        GL11.glRotatef((float)mod_DWCM.entityTardis.ticksExisted*.6F, 0, 0, 1);
        RenderUtils.drawTexturedModalRect(-48, -48, 128, 0, 96, 96, 0, RenderUtils.SHEET_FACTOR_256);

        GL11.glPushMatrix();
        GL11.glScaled(.55, .55, 0);
        GL11.glTranslatef(45*(float)Math.cos((float)mod_DWCM.entityTardis.ticksExisted/45),
        		45*(float)Math.sin((float)mod_DWCM.entityTardis.ticksExisted/45), 0);
        GL11.glRotatef((float)mod_DWCM.entityTardis.ticksExisted*-3, 0, 0, 1);
        RenderUtils.drawTexturedModalRect(-32, -32, 0, 32, 64, 64, 0, RenderUtils.SHEET_FACTOR_256);
        GL11.glPopMatrix();

        GL11.glPopMatrix();
    }*/

	public static void renderCircle2(TextureManager renderEngine, int x, int y, float counter, float absSpeed){
		Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("dwcm:textures/gui/guiTardisFlight2_512.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, 0);
		float scale = 0.4F;
		GL11.glScalef(scale, scale, scale);
		absSpeed *= 1.5;

		GL11.glTranslatef(-8*absSpeed*0.1f, 0, 0);
		GL11.glPushMatrix();
		GL11.glRotatef(counter, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-88, -88, 0, 0, 176, 176, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glTranslatef(-48*absSpeed*0.3f, 8*absSpeed*0.3f, 0);
		GL11.glPushMatrix();
		GL11.glRotatef(-counter*0.8f, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-152, -152, 208, 0, 304, 304, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glTranslatef(-56*absSpeed*0.6f, -8*absSpeed*0.5f, 0);
		GL11.glPushMatrix();
		GL11.glRotatef(counter*1.2f, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-88, -88, 0, 176, 176, 176, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glTranslatef(-60*absSpeed*0.8f, 14*absSpeed*0.7f, 0);
		GL11.glPushMatrix();
		GL11.glRotatef(counter, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-80, -80, 0, 352, 160, 160, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glTranslatef(-50*absSpeed, -32*absSpeed-4, 0);
		GL11.glPushMatrix();
		GL11.glRotatef(-counter*0.4f, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-104, -104, 256, 304, 208, 208, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glPopMatrix();
	}

	public static void renderCircle3(TextureManager renderEngine, int x, int y, float counter, float absSpeed){
		Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("dwcm:textures/gui/symbol1_1.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, 0);
		float scale = 0.19F;
		GL11.glScalef(scale, scale, scale);

		GL11.glPushMatrix();
		RenderUtils.drawTexturedModalRect(-215, -235, 0, 0, 430, 470, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("dwcm:textures/gui/symbol1_2.png"));
		GL11.glPushMatrix();
		GL11.glRotatef(-counter*0.6f, 0, 1, 0);
		RenderUtils.drawTexturedModalRect(7, 22, 0, 0, 82, 72,  0, RenderUtils.SHEET_FACTOR_256);
		RenderUtils.drawTexturedModalRect(-50, -44, 0, 158, 86, 36, 0, RenderUtils.SHEET_FACTOR_256);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glRotatef((180+counter)*0.9f, 0, 1, 0);
		RenderUtils.drawTexturedModalRect(-68, 12, 0, 112, 118, 48, 0, RenderUtils.SHEET_FACTOR_256);
		RenderUtils.drawTexturedModalRect(6, -80, 112, 0, 100, 100, 0, RenderUtils.SHEET_FACTOR_256);
		GL11.glPopMatrix();

		GL11.glPopMatrix();
	}

	public static void renderCircle4(TextureManager renderEngine, int x, int y, float counter, float absSpeed){
		Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("dwcm:textures/gui/symbol1_2.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, 0);
		float scale = 0.52F;
		GL11.glScalef(scale, scale, scale);

		GL11.glRotatef(counter*0.45f+(absSpeed*12), 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-48, -48, 0, 0, 96, 96, 0, RenderUtils.SHEET_FACTOR_128);

		GL11.glPopMatrix();
	}
	
	public static void renderCircle5(TextureManager renderEngine, int x, int y, float counter, float absSpeed){
		Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("dwcm:textures/gui/guiTardisFlight2_512.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, 0);
		float scale = 0.4F;
		GL11.glScalef(scale, scale, scale);
		absSpeed *= 1.5;

		GL11.glPushMatrix();
		GL11.glTranslatef(-20, 0, 0);
		GL11.glScalef(1.5f, 1.5f, 1f);
		GL11.glRotatef(counter, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-88, -88, 0, 0, 176, 176, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glTranslatef(-48*absSpeed*0.3f, 8*absSpeed*0.3f, 0);
		GL11.glPushMatrix();
		GL11.glRotatef(-counter*0.8f, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-152, -152, 208, 0, 304, 304, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();
		
		GL11.glPopMatrix();
	}
	public static void renderCircle6(TextureManager renderEngine, int x, int y, float counter, float absSpeed){
		Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation("dwcm:textures/gui/guiTardisFlight2_512.png"));
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, 0);
		float scale = 0.625F;
		GL11.glScalef(scale, scale, scale);

		GL11.glPushMatrix();
		GL11.glScalef(1.15f, 1.15f, 1f);
		GL11.glRotatef(counter, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-80, -80, 0, 352, 160, 160, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glRotatef(-counter*0.4f, 0, 0, 1);
		RenderUtils.drawTexturedModalRect(-104, -104, 256, 304, 208, 208, 0, RenderUtils.SHEET_FACTOR_512);
		GL11.glPopMatrix();

		GL11.glPopMatrix();
	}
}

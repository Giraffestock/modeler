package com.ikingssgc.mdc.gui.elements;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import com.ikingssgc.mdc.utils.Utils;

public class GraphicButton extends GuiButton {

    private String specialTex;
    private boolean useWholeSheet = false;
    private float widthFactor = 1.0F / 256.0F;
    private float heightFactor = 1.0F / 256.0F;
    private int u = 0;
    private int v = 0;
    private float scale = 1.0f;
    private int textColor = 14737632;

    public GraphicButton(int id, int posX, int par3, String text) {
        super(id, posX, par3, text);
    }

    public GraphicButton(int id, int posX, int posY, int width, int height,
            String text) {
        super(id, posX, posY, width, height, text);

    }

    public GraphicButton(int id, int posX, int posY, int w, int h, String tex, String text) {
        super(id, posX, posY, w, h, text);

        /*this.w = w;
         this.h = h;*/
        this.specialTex = tex;
    }

    public GraphicButton setUV(int u, int v) {
        this.u = u;
        this.v = v;
        return this;
    }

    public GraphicButton useWholeSheet() {
        this.useWholeSheet = true;
        return this;
    }

    public GraphicButton setTextColor(int color) {
        this.textColor = color;
        return this;
    }

    public GraphicButton setScale(float f) {
        this.scale = f;
        this.width *= scale;
        this.height *= scale;
        return this;
    }

    @Override
    public void drawButton(Minecraft par1Minecraft, int par2, int par3) {
        if (this.visible) {
            FontRenderer var4 = par1Minecraft.fontRenderer;
            par1Minecraft.renderEngine.bindTexture(new ResourceLocation(this.specialTex));
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            boolean var5 = par2 >= this.xPosition && par3 >= this.yPosition && par2 < this.xPosition + this.width && par3 < this.yPosition + this.height;
            int var6 = this.getHoverState(var5);
            this.drawTexturedModalRect(this.xPosition, this.yPosition, u, v, width, height);
            this.mouseDragged(par1Minecraft, par2, par3);
            int var7 = this.textColor;

            if (!this.enabled) {
                var7 = -6250336;
            } else if (var5) {
                var7 = 16777120;
            }

            this.drawCenteredString(var4, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, var7);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        }
    }

    public void drawCenteredStringNoShadow(FontRenderer par1FontRenderer, String par2Str, int par3, int par4, int par5) {
        par1FontRenderer.drawString(par2Str, par3 - par1FontRenderer.getStringWidth(par2Str) / 2, par4, par5);
    }

    public void drawTexturedModalRect(int x, int y, int u, int v, int width, int height) {
        Tessellator tess = Tessellator.instance;
        if (this.useWholeSheet) {
            Utils.drawWholeSheet(x, y, width, height, this.zLevel);
        } else {
            tess.startDrawingQuads();
            tess.addVertexWithUV((double) (x + 0), (double) (y + height), (double) this.zLevel, (double) ((float) (u + 0) * widthFactor), (double) ((float) (v + height) * heightFactor));
            tess.addVertexWithUV((double) (x + width), (double) (y + height), (double) this.zLevel, (double) ((float) (u + width) * widthFactor), (double) ((float) (v + height) * heightFactor));
            tess.addVertexWithUV((double) (x + width), (double) (y + 0), (double) this.zLevel, (double) ((float) (u + width) * widthFactor), (double) ((float) (v + 0) * heightFactor));
            tess.addVertexWithUV((double) (x + 0), (double) (y + 0), (double) this.zLevel, (double) ((float) (u + 0) * widthFactor), (double) ((float) (v + 0) * heightFactor));
            tess.draw();
        }
    }
}

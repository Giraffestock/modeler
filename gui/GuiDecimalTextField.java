package com.ikingssgc.mdc.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;

/**
 *
 * @author Emir
 */
public class GuiDecimalTextField extends GuiTextField {

    public GuiDecimalTextField(FontRenderer par1FontRenderer, int par2, int par3, int par4, int par5) {
        super(par1FontRenderer, par2, par3, par4, par5);
    }

    @Override
    public void writeText(String text) {
        StringBuilder ns = new StringBuilder();
        for (char c : text.toCharArray()) {
            if (Character.isDigit(c) || c == '.') {
                ns.append(c);
            }
        }
        super.writeText(ns.toString());
    }

    public double getValue() {
        return Double.parseDouble(getText());
    }

}

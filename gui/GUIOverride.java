package com.ikingssgc.mdc.gui;

import cpw.mods.fml.common.ObfuscationReflectionHelper;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiVideoSettings;
import net.minecraftforge.client.event.GuiOpenEvent;

public class GUIOverride {

    @SubscribeEvent
    public void overrideGui(GuiOpenEvent event) {
        if (event.gui instanceof GuiOptions) {
            GuiScreen parent = ObfuscationReflectionHelper.getPrivateValue(GuiOptions.class, (GuiOptions) event.gui, 1);
            event.gui = new GuiOptionsProxy(parent, Minecraft.getMinecraft().gameSettings);
        }
    }
}

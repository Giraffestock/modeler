package com.ikingssgc.mdc.gui;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.HashMap;
import java.util.Map;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlot;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.EnumChatFormatting;
import org.lwjgl.opengl.GL11;

public class GuiSlotConfig extends GuiSlot {

    private final GuiConfig parent;
    private final Minecraft mc;
    private final JsonObject json;
    protected final Map<String, JsonPrimitive> jsonMap = new HashMap<String, JsonPrimitive>();
    protected final Map<Integer, String> jsonIntegerMap = new HashMap<Integer, String>();
    protected int selected = -1;
    private final int width2, height2;

    public GuiSlotConfig(GuiConfig parent, Minecraft mc, JsonObject json, int width2, int height2) {
        super(mc, parent.width, parent.height, 30, (parent.height - 40) + 4, 25);
        this.mc = mc;
        this.parent = parent;
        this.json = json;
        this.width2 = width2;
        this.height2 = height2;

        for (Map.Entry<String, JsonElement> entry : json.entrySet()) {
            if (entry.getValue().isJsonPrimitive()) {
                jsonMap.put(entry.getKey(), entry.getValue().getAsJsonPrimitive());
            }
        }

        int i = 0;
        for (Map.Entry<String, JsonPrimitive> entry : jsonMap.entrySet()) {
            jsonIntegerMap.put(i, entry.getKey());
            i++;
        }
    }

    @Override
    protected int getSize() {
        return json.entrySet().size();
    }

    @Override
    protected boolean isSelected(int var1) {
        return var1 == selected;
    }

    @Override
    protected void drawBackground() {
    }

    @Override
    public void drawScreen(int par1, int par2, float par3) {
        super.drawScreen(par1, par2, par3);
    }

    @Override
    protected void drawSlot(int index, int xPosition, int yPosition, int l, Tessellator tessellator, int var6, int var7) {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        String name = jsonIntegerMap.get(index);
        JsonPrimitive primitive = jsonMap.get(name);

        parent.drawString(mc.fontRenderer, name.replaceAll("_", " ") + " : " + EnumChatFormatting.DARK_GREEN.toString() + primitive.toString(), xPosition + 4, yPosition + 6, 0xFFFFFFFF);
    }

    @Override
    protected int getScrollBarX() {
        return (this.width2 / 2) + 164;
    }

    @Override
    protected void elementClicked(int index, boolean var2, int var3, int var4) {
        if (!var2) {
            if (selected == -1) {
                selected = index;
                parent.initInput(index);
            }
        }
    }
}

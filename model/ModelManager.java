package com.ikingssgc.mdc.debug.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.model.ModelBase;
import net.minecraft.util.ResourceLocation;

public class ModelManager {
	
	private static ArrayList<RenderableModel> MODELS = new ArrayList<RenderableModel>();
	private static Map<Class<? extends ModelBase>, Integer> CLASS_TO_ID = new HashMap<Class<? extends ModelBase>, Integer>();
	private static Map<String, Integer> NAME_TO_ID = new HashMap<String, Integer>();
	
	public static int regModel(String name, ModelBase mb, ResourceLocation tex){
		int id = MODELS.size();
		
		RenderableModel rm = new RenderableModel(name, mb, tex); 
		MODELS.add(rm);
		CLASS_TO_ID.put(mb.getClass(), id);
		NAME_TO_ID.put(name.toLowerCase(), id);
		return id;
	}
	
	public static RenderableModel GetRenderModel(String name){
		if(NAME_TO_ID.containsKey(name))
			return GetRenderModel(NAME_TO_ID.get(name));
		return null;
	}
	
	public static RenderableModel GetRenderModel(int id){
		return MODELS.get(id);
	}
}

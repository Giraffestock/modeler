package com.ikingssgc.mdc.debug.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.util.ResourceLocation;

public class RenderableModel {

	private String name;
	private ModelBase mb;
	private ResourceLocation tex;
	
	public RenderableModel(String name, ModelBase mb, ResourceLocation tex){
		this.name = name;
		this.mb = mb;
		this.tex = tex;
	}
	
	public String getName() { return name; }
	public ModelBase getModel() { return mb; }
	public ResourceLocation getTexture() { return tex; }
}

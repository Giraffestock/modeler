package com.ikingssgc.mdc.debug.modeler.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.client.Minecraft;

import com.ikingssgc.mdc.container.ContainerBase;
import com.ikingssgc.mdc.debug.model.ModelBaseMDC;
import com.ikingssgc.mdc.debug.model.ModelPiece;
import com.ikingssgc.mdc.gui.GuiContainerBase;
import com.ikingssgc.mdc.gui.Listener;
import com.ikingssgc.mdc.gui.elements.Button;
import com.ikingssgc.mdc.gui.elements.CompositeScrolled;
import com.ikingssgc.mdc.gui.elements.GuiElement;
import com.ikingssgc.mdc.gui.elements.Label;

public class GuiPieceSelection extends GuiContainerBase
{

	CompositeScrolled area;
	GuiModelEditor parent;

	Label selectedPiece;

	Label selectedPieceDisplayLabel;

	ModelPiece excludedPiece;

	public GuiPieceSelection(GuiModelEditor parent)
	{
		super((ContainerBase) parent.inventorySlots, 256, 240, defaultBackground);  
		this.parent = parent;
	}

	public GuiPieceSelection(GuiModelEditor parent, ModelPiece excludedPiece)
	{
		super((ContainerBase) parent.inventorySlots, 256, 240, defaultBackground);  
		this.parent = parent;
		this.excludedPiece = excludedPiece;
	}

	@Override
	public void initElements()
	{
		area = new CompositeScrolled(0, 40, 256, 200);
		addGuiElement(area);

		selectedPieceDisplayLabel = new Label(8, 8, "No Selection");
		addGuiElement(selectedPieceDisplayLabel);

		Button button = new Button(256-8-55-55-4, 8, 55, 12,"Cancel")
		{
			@Override
			protected void onPressed()
			{
				((ContainerBase)parent.inventorySlots).setGui(parent);
				Minecraft.getMinecraft().displayGuiScreen(parent);
			}
		};
		addGuiElement(button);

		button = new Button(256-8-55, 8, 55, 12, "Done")
		{
			@Override
			protected void onPressed()
			{
				if(selectedPiece!=null)
				{
					onPieceSelected(pieceMap.get(selectedPiece));
					((ContainerBase)parent.inventorySlots).setGui(parent);
					Minecraft.getMinecraft().displayGuiScreen(parent);
				}
			}
		};
		addGuiElement(button);

		int totalHeight = 3;

		ModelBaseMDC model = parent.modelWidget.getModel();
		List<ModelPiece> pieces = new ArrayList<ModelPiece>(); 
		model.getPieces(pieces);

		Label label;
		for(ModelPiece piece : pieces)
		{
			if(piece==excludedPiece){continue;}
			label = new Label(3, totalHeight, piece.getName());
			label.addNewListener(new Listener(Listener.MOUSE_UP)
			{
				@Override
				public boolean onEvent(GuiElement widget, ActivationEvent evt)
				{
					if(widget.isMouseOverElement(evt.mx, evt.my))
					{
						selectedPiece = (Label)widget;
						selectedPieceDisplayLabel.setText("Selected Piece: "+pieceMap.get(widget).getName());
					}
					return true;
				}
			});
			area.addGuiElement(label);
			pieceMap.put(label, piece);
			totalHeight+=12;
		}  
	}

	private HashMap<Label, ModelPiece> pieceMap = new HashMap<Label, ModelPiece>();

	@Override
	public void setupElements()
	{

	}

	/**
	 * anonymous classes should override for piece-selection functionality
	 * @param piece
	 */
	protected void onPieceSelected(ModelPiece piece)
	{

	}

}
